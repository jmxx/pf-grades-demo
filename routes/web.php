<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/students', 'StudentsController@index')->name('students.index');

Route::get('/students/{user}/subjects', 'SubjectsController@create')->name('subjects.create');
Route::post('/students/{user}/subjects', 'SubjectsController@store')->name('subjects.store');
Route::put('/students/{user}/subjects', 'SubjectsController@update')->name('subjects.update');
Route::delete('/students/{user}/subjects', 'SubjectsController@destroy')->name('subjects.destroy');

Route::get('/students/{user}/subjects.json', 'SubjectsController@json')->name('subjects.json');
