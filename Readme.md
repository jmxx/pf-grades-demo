# Demo

- Clona el repositorio
- Instala las dependencias con `composer install`
- Copia el archivo `.env.example` como `.env` y configura las variables de la BD
- Inicia las migraciones `php artisan migrate --seed`
- Corre el servidor de artisan `php artisan serve`

URL de la demo: http://sample-env.ev47hycvde.us-west-2.elasticbeanstalk.com

Puedes registrar un usuario nuevo o iniciar sesión con:
u: yeah.mue@gmail.com
p: 123465

El archivo de rutas se encuentra aquí: https://gitlab.com/jmxx/pf-grades-demo/blob/master/routes/web.php

:)