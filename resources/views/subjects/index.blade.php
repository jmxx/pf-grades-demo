@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Calificaciones de {{ $student->name }}</div>

                <div class="panel-body">
                  <table class="table">
                    <thead>
                      <tr>
                        <th>Materia</th>
                        <th>Calificación</th>
                        <th>Actualizar</th>
                        <th>Borrar</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($student->subjects as $key => $subject)
                        <tr>
                          <td>{{ $subject->subject }}</td>
                          <td>{{ $subject->pivot->grade }}</td>
                          <td>
                            <form class="form-horizontal" role="form" method="POST" action="{{ route('subjects.update', [
                              'user_id' => $student->id
                              ]) }}">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                              <input type="hidden" name="grade_id" value="{{ $subject->pivot->id }}">
                              <input type="text" name="grade" value="">
                              <button type="submit" class="btn btn-primary">
                                  Actualizar
                              </button>
                            </form>
                          </td>
                          <td>
                            <form class="form-horizontal" role="form" method="POST" action="{{ route('subjects.destroy', [
                              'user_id' => $student->id
                              ]) }}">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                              <input type="hidden" name="grade_id" value="{{ $subject->pivot->id }}">
                              <button type="submit" class="btn btn-danger">
                                  Eliminar
                              </button>
                            </form>
                          </td>
                        </tr>
                      @endforeach
                      <tr>
                        <td>Promedio</td>
                        <td>{{ $average }}</td>
                        <td></td>
                        <td></td>
                      </tr>
                    </tbody>
                  </table>
                  <form class="form-horizontal" role="form" method="POST" action="{{ route('subjects.store', [
                    'user_id' => $student->id
                    ]) }}">
                      {{ csrf_field() }}

                      <div class="form-group{{ $errors->has('subject') ? ' has-error' : '' }}">
                          <label for="name" class="col-md-4 control-label">Materia</label>

                          <div class="col-md-6">
                            <select class="" name="subject">
                              @foreach ($subjects as $key => $subject)
                                <option value="{{ $subject->id }}">{{ $subject->subject }}</option>
                              @endforeach
                            </select>
                              {{-- <input id="subject" type="text" class="form-control" name="subject" value="{{ old('subject') }}" required autofocus> --}}

                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('subject') }}</strong>
                                </span>
                            @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('grade') ? ' has-error' : '' }}">
                          <label for="grade" class="col-md-4 control-label">Calificación</label>

                          <div class="col-md-6">
                              <input id="grade" type="text" class="form-control" name="grade" value="{{ old('grade') }}" required autofocus>

                              @if ($errors->has('grade'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('grade') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group">
                          <div class="col-md-6 col-md-offset-4">
                              <button type="submit" class="btn btn-primary">
                                  Guardar calificación
                              </button>
                          </div>
                      </div>
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
