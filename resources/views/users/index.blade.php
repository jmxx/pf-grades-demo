@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Students</div>

                <div class="panel-body">
                  <table class="table">
                    <thead>
                      <tr>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Activo</th>
                        <th>Email</th>
                        <th>Acciones</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($students as $key => $student)
                        <tr>
                          <td>{{ $student->name }}</td>
                          <td>{{ $student->lastname }}</td>
                          <td>{{ $student->active }}</td>
                          <td>{{ $student->email }}</td>
                          <td>
                            <a href="{{ route('subjects.create', [
                              'user_id' => $student->id
                              ])}}">Registrar calificación</a>
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
