<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Subject;
use App\Grade;

class SubjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(User $user)
    {
      $subjects = Subject::whereNotIn('id', $user->subjects->pluck('id')->toArray())
        ->get();

      return view('subjects.index', [
        'student' => $user,
        'subjects' => $subjects,
        'average' => $user->subjects->avg('pivot.grade')
      ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
      $grade = Grade::create([
        'student_id' => $user->id,
        'subject_id' => $request->subject,
        'grade' => $request->grade
      ]);

      return [
        'success' => 'ok',
        'msg' => 'calificacion registrada'
      ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $id)
    {
      $grade = Grade::where('id', $request->grade_id)->first();

      $grade->grade = $request->grade;

      $grade->save();

      return [
        "success" => "ok",
        "msg" => "calificacion actualizada"
      ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, User $id)
    {
      Grade::destroy($request->grade_id);

      return [
        "success" => "ok",
        "msg" => "calificacion eliminada"
      ];
    }

    public function json(User $user) {
      $response = [
      ];

      foreach ($user->subjects as $key => $subject) {
        $response[] = [
          'student_id'=> $user->id,
          'name'=> $user->name,
          'lastname'=> $user->lastname,
          'subject' => $subject->subject,
          'grade' => $subject->pivot->grade,
          'date' =>  $subject->pivot->created_at->format('d/m/Y')
        ];
      }

      $response[] = [
        'grade' => $user->subjects->avg('pivot.grade')
      ];

      return $response;
    }
}
