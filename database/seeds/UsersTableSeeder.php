<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
        'name' => 'Juan Manuel',
        'lastname' => 'Reynoso',
        'email' => 'mue@gmail.com',
        'password' => bcrypt('123456'),
      ]);

      DB::table('users')->insert([
        'name' => 'John',
        'lastname' => 'Doe',
        'email' => 'john.doe@gmail.com',
        'password' => bcrypt('123456'),
      ]);
    }
}
