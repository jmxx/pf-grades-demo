<?php

use Illuminate\Database\Seeder;

class SubjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('subjects')->insert([
        'subject' => 'Matematicas',
        'active' => true,
      ]);

      DB::table('subjects')->insert([
        'subject' => 'Programación',
        'active' => true,
      ]);

      DB::table('subjects')->insert([
        'subject' => 'Ingeniería de Software',
        'active' => true,
      ]);
    }
}
